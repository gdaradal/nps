﻿using bcsexam.Data.Models;
using bcsexam.Data.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam.Services
{
	public class CustomerService : ICustomerService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IRepository<Talktoguest> talkToGuestRepo;
		private readonly IRepository<SpokenToGuest> spokenToGuestRepo;

		public CustomerService(
				IUnitOfWork unitOfWork,
				IRepository<Talktoguest> talkToGuestRepo, 
				IRepository<SpokenToGuest> spokenToGuestRepo
			)
		{
			this.unitOfWork = unitOfWork;
			this.talkToGuestRepo = talkToGuestRepo;
			this.spokenToGuestRepo = spokenToGuestRepo;
		}

		public IEnumerable<CustomerDto> GetCustomer(string parkCode, string arriving)
		{
			try
			{
				var customer = talkToGuestRepo.All();
				var spoken = spokenToGuestRepo.All();

				var data =
					(
					from c in customer
					where c.ParkCode == parkCode && c.Arrived == arriving
						  && !(from s in spoken select s.ResId).Contains(c.ResId)
					select new CustomerDto
					{
						AreaName = c.AreaName,
						Arrived = c.Arrived,
						Category = c.Category,
						Depart = c.Depart,
						GuestMobile = c.GuestMobile,
						GuestName = c.GuestName,
						Nights = c.NightsThisRes,
						PreviousNPCComment = c.PrevNpsComment,
						PreviousNPS = string.IsNullOrEmpty(c.PrevNps) ? 0 : int.Parse(c.PrevNps),
						ReservationId = c.ResId
					}
					).ToList();

				return data;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public void RecordReponse(string resId, string email)
		{
			try
			{
				var response = new SpokenToGuest();

				response.ResId = resId;
				response.UserEmail = email;

				// !!!! there is wrong in the spokenToGuest table
				// there is no primary key define or the EF to track

				spokenToGuestRepo.Insert(response);
				this.unitOfWork.Save();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
