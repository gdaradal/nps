﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam.Services
{
    public class CustomerDto
    {
        public string ReservationId { get; set; }
        public string GuestName { get; set; }
        public string GuestMobile { get; set; }
        public string Arrived { get; set; }
        public string Depart { get; set; }
        public string Category { get; set; }
        public string Nights { get; set; }
        public string AreaName { get; set; }
        public int? PreviousNPS { get; set; }
        public string PreviousNPCComment { get; set; }
    }

}
