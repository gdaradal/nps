﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam.Services
{
	public interface ICustomerService
	{
		IEnumerable<CustomerDto> GetCustomer(string parkCode, string arriving);
		void RecordReponse(string resId, string email);
	}
}
