﻿using bcsexam.Data;
using bcsexam.Data.Repository;
using bcsexam.Data.Repository.Interface;
using bcsexam.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            // Configure DbContext with Scoped lifetime
            services.AddDbContext<employeetestingdbContext>(options =>
            {
                var cnstring = configuration.GetConnectionString("bcsexam");

                options.UseSqlServer(cnstring);
                options.UseLazyLoadingProxies();
            }
            );

            services.AddScoped<Func<employeetestingdbContext>>((provider) => () => provider.GetService<employeetestingdbContext>());
            services.AddScoped<DbFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            return services;
        }


        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerService, CustomerService>();
        }
    }
}
