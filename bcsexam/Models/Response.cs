﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam.Models
{
    public class ResponseInsertUpdateModel
    {
		[Required(ErrorMessage = "Res Id Required. ")]
		public string ResId { get; set; }
		[Required(ErrorMessage = "User Email Required. ")]
		public string UserEmail { get; set; }

	}

}
