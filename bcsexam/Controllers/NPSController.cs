﻿using bcsexam.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bcsexam.Models;

namespace bcsexam.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class NPSController : ControllerBase
	{
		private readonly ICustomerService customerSvc;

		public NPSController(ICustomerService customerSvc)
		{
			this.customerSvc = customerSvc;
		}

		[HttpGet]
		[Route("Customers")]
		public IActionResult Customers(string parkCode, string arriving) {
			try
			{
				// specific return string indicated in the problem
				if (string.IsNullOrEmpty(parkCode)) throw new Exception("Please supply Park Code");
				if (string.IsNullOrEmpty(parkCode)) throw new Exception("Please supply arriving");

				var data = customerSvc.GetCustomer(parkCode, arriving);
				return Ok(data);

			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}

		[HttpPost]
		[Route("Response")]
		public IActionResult GetResponse(ResponseInsertUpdateModel model ) {

			try
			{
				if (!ModelState.IsValid) {
					var ExceptionMessage = "ERROR: ";
					foreach (var value in ModelState.Values)
					{
						ExceptionMessage += " " + value.Errors[0].ErrorMessage;
					}
					throw new Exception(ExceptionMessage);
				}

				// !!! will error because the spokenToGuest table doesn't have a primary key defined
				customerSvc.RecordReponse(model.ResId, model.UserEmail);
				return Ok();
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
			
		}
	}
}
