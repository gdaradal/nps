﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace bcsexam.Data.Models
{
    public partial class SpokenToGuest
    {
        [Key]
        public string ResId { get; set; }
        public string UserEmail { get; set; }
    }
}
