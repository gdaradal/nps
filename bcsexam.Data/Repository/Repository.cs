﻿using bcsexam.Data.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace bcsexam.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly DbFactory _dbFactory;
        private DbSet<TEntity> _dbSet;
        protected DbSet<TEntity> DbSet
        {
            get => _dbSet ?? (_dbSet = _dbFactory.DbContext.Set<TEntity>());
        }
        public Repository(DbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public void Insert(TEntity entity)
        {

            DbSet.Add(entity);
        }
        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }
        public void Delete(TEntity entity)
        {
                DbSet.Remove(entity);
        }
        public IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> expression)
        {
            return DbSet.Where(expression).AsQueryable<TEntity>();
        }
        public IQueryable<TEntity> All()
        {
            return DbSet.AsQueryable();
        }
        public TEntity GetByID(object id)
        {
            return DbSet.Find(id);
        }
    }
}
