﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bcsexam.Data.Repository.Interface
{
    public interface IUnitOfWork
    {
        void Save();
    }

}
